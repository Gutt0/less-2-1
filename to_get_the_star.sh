#!/bin/bash

FAILED_PIPELINE_IDS=$(curl -s --header "PRIVATE-TOKEN: $MY_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines" | jq '.[] | select(.status == "failed") | .id')

FOUND_ID=false

for FAILED_PIPELINE_ID in $FAILED_PIPELINE_IDS; do
    if [[ -z $(curl -s --header "PRIVATE-TOKEN: $MY_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$FAILED_PIPELINE_ID/jobs" | jq '.[] | select(.name == "build_prod") | select(.status == "failed")') ]]; then
        continue
    fi
    FOUND_ID=true
    echo "failed 'build_prod' ID: $FAILED_PIPELINE_ID"
    break
done

if [ "$FOUND_ID" = false ]; then
    echo "No failed job 'build_prod'"
fi