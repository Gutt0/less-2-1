JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $MY_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/45858705/pipelines/$CI_PIPELINE_ID/jobs" | jq '.[] | select(.name == "triggered_job") | .id')
curl -k --request POST --header "PRIVATE-TOKEN: $MY_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/45858705/jobs/$JOB_ID/play"
